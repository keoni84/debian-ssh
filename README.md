# README #

Base repository for debian-ssh image

### What is this repository for? ###

* Base debian:latest with sshd

### To run with sshd on port 2222 ###

* docker run -p 2222:22 -d -t -i -v /sys/fs/cgroup:/sys/fs/cgroup:ro keoni84/debian-ssh

### Login ###

* User: root
* Password: root